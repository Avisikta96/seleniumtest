Feature: Validate functionalities on dashboard page

@FacebookRunner
  Scenario Outline: user wants to submit a payment
    Given user logs in "IP_Url","IP_Userid","IP_Password"
    When user reaches dashboard page
    Then user should be able to submit payment

    Examples: 
      | IP_Url                | IP_Userid                   | IP_Password |
      | https://facebook.com/ | nikiavisiktapanda@gmail.com | success     |
