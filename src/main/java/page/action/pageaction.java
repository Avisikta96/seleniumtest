package page.action;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import page.locator.locator;

public class pageaction {

locator q=new locator();
	
	public void launchapp(String url) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		driver.get(url);
		
	}

	public void provideuserid(String userid) {
		
		q.practiceuserid.click();
		q.practiceuserid.sendKeys(userid);
	}

	public void providepassword(String password) {
		
		q.practicepassword.click();
		q.practicepassword.sendKeys(password);
	}
}
