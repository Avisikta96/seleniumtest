package page.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
public class locator {
	@FindBy(xpath="\\input[@name='email']")
	public WebElement practiceuserid;

	@FindBy(xpath="\\input[@name='pass']")
	public WebElement practicepassword;

	public locator()
	{
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		PageFactory.initElements(driver, this);
	}
}
