package stepdef;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import page.action.pageaction;

public class stepdefinition {


	pageaction p=new pageaction();
	
	@Given("^user logs in \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void user_logs_in(String url,String userid, String password) throws Throwable{
		 
		p.launchapp(url);
		p.provideuserid(userid);
		p.providepassword(password);
	}
	
	@When("^user reaches dashboard page$")
	public void user_reaches_dashboard() throws Throwable{
		//call 
	}
	@Then("^user should be able to submit payment$")
	public void user_does_payment() throws Throwable{
		//call
	}
}
